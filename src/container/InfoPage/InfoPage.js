import React, {Component} from 'react';
import axios from "../../axios-page";
import ErrorBoundary from '../../components/ErrorBoundary/ErrorBoundary'
import widthErrorHandler from "../../HOC/widthErrorHandler/widthErrorHandler";


class InfoPage extends Component {

    state = {
        title: '',
        content: '',
    };

    pageData() {
        const name = this.props.match.params.name;
        axios.get(`pages/${name}.json`).then(response => {
            console.log(response);
            this.setState({title: response.data.title, content: response.data.content})
        });
    }

    componentDidMount() {
        this.pageData()
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if(this.props.match.params.name !== prevProps.match.params.name) {
            this.pageData()
        }
    }

    render() {
        return (
            <ErrorBoundary key={this.state.id}>
                <div className='info'>
                    <h1>{this.state.title}</h1>
                    <p>{this.state.content}</p>
                </div>
            </ErrorBoundary>
        );
    }
}

export default widthErrorHandler(InfoPage, axios);