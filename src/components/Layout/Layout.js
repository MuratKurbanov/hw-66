import React, {Fragment} from 'react';
import './Layout.css';
import Toolbar from '../Navigation/Toolbar/Toolbar';

const Layout = ({children}) => {
    return (
        <Fragment>
            <Toolbar/>
            <main className='Layout-Content'>
                {children}
            </main>
        </Fragment>
    )
};

export default Layout;