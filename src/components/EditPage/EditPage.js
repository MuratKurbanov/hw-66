import React, {Component, Fragment} from 'react';
import axios from '../../axios-page';
import {CATEGORIES} from "../../constant";

class EditPage extends Component {

    state = {
        category: 'home',
        title: '',
        content: ''
    };

    componentDidMount() {
        axios.get(`/pages/${this.state.category}.json`).then(response => {
            this.setState({
                title: response.data.title,
                content: response.data.content
            })
        })
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if(this.state.category !== prevState.category) {
            axios.get(`/pages/${this.state.category}.json`).then(response => {
                this.setState({
                    title: response.data.title,
                    content: response.data.content
                })
            })
        }
    }

    changeHandler = event => {
        this.setState({[event.target.name]: event.target.value});
    };

    editPage = (event) => {
        event.preventDefault();
        const data = {
            title: this.state.title,
            content: this.state.content
        };
        axios.put(`pages/${this.state.category}.json`, data).then(() => {
            this.props.history.replace('/pages/home');
        });
    };

    render() {
        return (
            <Fragment>
                <h2>Edit Page</h2>
                <form onSubmit={this.editPage}>
                    <select name="category" onChange={this.changeHandler} value={this.state.category}>
                        {Object.keys(CATEGORIES).map((categoryId, index) =>(
                            <option key={index} value={categoryId}>{CATEGORIES[categoryId]}</option>
                        ))}
                    </select>
                    <div>
                        <p>Title</p>
                        <input type="text" name='title'
                               value={this.state.title} onChange={this.changeHandler}
                        />
                    </div>
                    <div>
                        <p>Content</p>
                        <input type="text" name='content'
                               value={this.state.content} onChange={this.changeHandler}
                        />
                    </div>
                    <div>
                        <button type='submit'>Save</button>
                    </div>
                </form>
            </Fragment>
        );
    }
}

export default EditPage;