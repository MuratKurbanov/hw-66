import React from 'react';
import './NavigationPages.css'
import NavigationPage from "../NavigationPage/NavigationPage";

const NavigationPages = () => {
    return (
        <div className='NavigationPages'>
            <NavigationPage to='/pages/home' exact>Home</NavigationPage>
            <NavigationPage to='/pages/react' exact>React</NavigationPage>
            <NavigationPage to='/pages/about' exact>About</NavigationPage>
            <NavigationPage to='/pages/contacts' exact>Contacts</NavigationPage>
            <NavigationPage to='/pages/division' exact>Division</NavigationPage>
            <NavigationPage to='/pages/admin/edit' exact>Admin</NavigationPage>

        </div>
    );
};

export default NavigationPages;