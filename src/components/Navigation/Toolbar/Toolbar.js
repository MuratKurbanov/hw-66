import React from 'react';
import './Toolbar.css'
import Navigation from '../NavigationPages/NavigationPages'

const Toolbar = () => {
    return (
        <header className='Toolbar'>
            <h1>Static Page</h1>
            <nav>
                <Navigation />
            </nav>
        </header>
    )
};

export default Toolbar;