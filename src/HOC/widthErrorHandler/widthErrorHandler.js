import React, {Component, Fragment} from 'react';
import Modal from "../../components/Modal/Modal";
import Spinner from "../../components/Spinner/Spinner";

const withErrorHandler = (WrappedComponent, axios) => {
    return class WithErrorHOC extends Component {

        constructor(props) {
            super(props);

            this.state = {
                loading: false,
                error: null
            };

            axios.interceptors.request.use(req => {
                this.setState({loading: true});
                return req
            });

            this.state.interceptorId = axios.interceptors.response.use(res => {
                this.setState({loading: false});
                return res
            }, error => {
                this.setState({error});
                throw error;
            });
        }

        componentWillUnmount() {
            axios.interceptors.response.eject(this.state.interceptorId);
        }

        errorDismissed = () => {
            this.setState({error: null})
        };

        render() {
            return (
                <Fragment>
                    <Modal show={this.state.error} close={this.errorDismissed}>
                        {this.state.error && this.state.error.message}
                    </Modal>
                    {this.state.loading? <Spinner/> : null}
                    <WrappedComponent {...this.props}/>
                </Fragment>
            )
        }
    };
};

export default withErrorHandler;