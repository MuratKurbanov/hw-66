import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://js-3-page.firebaseio.com/'
});

export default instance;